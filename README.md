# WestWind ESF
Built on top of the Symfony Framework. Doing a lot of basic stuff that almost any site would need.

A sort of core system, I could have called it that. But I wanted something with which I can build many different sites, such as
  * a Blogging Site
  * a Community Site
  * a News Site
  * a Shop Site
  * a WHATEVER Site

  There are certain things that all sites need.



  -----

## Planned and Wanted Features
 * Multilingual (i18n) Interface
 * Multilingual (i18n) Content
 * A Pages system for various types thar don't chang often, but can.
   * About Page
   * Policy Pages
   * Legal Terms
 * Contact Page
 * European Cookie Law conformity


----

## Licence
[MIT](LICENSE) License.

## Documentation
During development I will hardly document this, as I have something simular in the past and docuntented those at [my tutorial site.](http://tutorials.mustangx.org)

## More Info
May be found at [the Spacewax site](http://spacewax.net), as soon as I find time to set it up.


### Screenshot(s)
 Here's a shot from 8 Jul 2018, with Bootstrap3 & FontAwesome 4.7.0: <br>
<img src="public/images/screenshots/screenshot-westwindESF_2018-07-08_01-39-39.png" />
